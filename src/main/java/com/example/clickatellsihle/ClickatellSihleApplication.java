package com.example.clickatellsihle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClickatellSihleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClickatellSihleApplication.class, args);
    }

}
